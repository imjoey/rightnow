#!/usr/bin/env python
# -*- coding: utf-8 -*-


from flask import Flask

app = Flask(__name__)

# #################load flask app configuration from cfg file and environments
app.config.from_pyfile('app.cfg')


from os import environ

if 'APP_NAME' in environ:
    from sae.const import MYSQL_USER, MYSQL_PASS, MYSQL_HOST, \
        MYSQL_PORT, MYSQL_DB
else:
    MYSQL_USER, MYSQL_PASS = 'root', 'qwer1234'
    MYSQL_HOST, MYSQL_PORT, MYSQL_DB = '192.168.59.103', '3306', 'rightnow'



mysql_uri = 'mysql://%s:%s@%s:%s/%s' % \
    (MYSQL_USER, MYSQL_PASS, MYSQL_HOST, MYSQL_PORT, MYSQL_DB)

app.config['SQLALCHEMY_DATABASE_URI'] = mysql_uri
app.secret_key = app.config['SECRET_KEY']


# ################# init the flask-sqlalchemy module #################
from flask_sqlalchemy import SQLAlchemy
#: Flask-SQLAlchemy extension instance
db = SQLAlchemy(app)




