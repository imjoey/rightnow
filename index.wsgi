import os
import sys

root = os.path.dirname(__file__)

sys.path.insert(0, os.path.join(root, 'site-packages.zip'))


from run import app

@app.route('/test')
def test():
    return 'env is ok'

import sae
application = sae.create_wsgi_app(app)


