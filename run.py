#!/usr/bin/env python
# -*- coding: utf-8 -*-

from server import app
from awards import draw_award, get_award


app.add_url_rule('/draw', 'draw_award', draw_award)
app.add_url_rule('/get', 'get_award', get_award)

#app.run(host='0.0.0.0')
