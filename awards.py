#!/usr/bin/evn python
# -*- coding: utf-8 -*-

from datetime import datetime
from flask import request, session, jsonify
from util import jsonp, random_pick
from server import db


class Awards(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, index=True, nullable=False)
    count = db.Column(db.Integer)
    probability = db.Column(db.Float)

    def __init__(self):
        pass

    def __repr__(self):
        return '<Award %s>' % self.name


class AwardLogs(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    awardname = db.Column(db.String(100))
    phone = db.Column(db.Integer)
    createtime = db.Column(db.DateTime)

    def __init__(self, awardname, phone):
        self.awardname = awardname
        self.phone = phone
        self.createtime = datetime.now()

    def __repr__(self):
        return '<AwardLog %s-%s>' % (self.phone, self.awardname)


awards_type = []
awards_probability = []
awards_dict = {}

award_list = Awards.query.all()
for award in award_list:
    awards_type.append(award.id)
    awards_probability.append(award.probability)
    awards_dict[award.id] = award.name



# app.route('/draw')
@jsonp
def draw_award():
    awardid = random_pick(awards_type, awards_probability)
    award = Awards.query.filter_by(id=awardid).first()

    has_bigone = 'drawed' in session and int(session['drawed']) < 4
    if has_bigone or award.count == 0:
        awardid = 4
        ebook_award = Awards.query.filter_by(id=awardid).first()
        if ebook_award.count == 0:
            return str(0)

    award.count = award.count - 1
    try:
        db.session.add(award)
        db.session.commit()
    except:
        return str(-1)

    session['drawed'] = awardid
    return str(awardid)

draw_award.methods = ['GET']



# app.route('/get')
@jsonp
def get_award():
    phone_no = request.args.get('phone')
    if 'drawed' not in session or phone_no:
        return str(0)
    award_log = AwardLogs(awards_dict[int(session['drawed'])], phone_no)
    try:
        db.session.add(award_log)
        db.session.commit()
    except Exception, e:
        print e
        return str(0)
    return str(1)

get_award.methods = ['GET']

